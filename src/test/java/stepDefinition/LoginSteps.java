package stepDefinition;

import bettingapp.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import qa.factory.DriverFactory;

public class LoginSteps {
	private static String title;
	private LoginPage loginPage = new LoginPage(DriverFactory.getDriver());

	@Given("user is on Home Page")
	public void user_is_on_home_page() {
		System.out.println("Step 1: User is on Search Page");
		DriverFactory.getDriver().get("https://www.bbc.co.uk");

		title = loginPage.getPageTitle();
		System.out.println("Page title is: " + title);
	}

	@When("user click sign in Link on Home Page")
	public void user_click_sign_in_link_on_home_page() {
		System.out.println("Step 2: User click on Sign In");
		loginPage.clickOnSignIN();
	}

	@And("Fill UserID field with {string} and Fill Password field with {string} from list and click Login on LoginPage")
	public void fill_user_id_field_with_and_fill_password_field_with_from_list_and_click_login_on_login_page(
			String username, String passwd) {
		System.out.println("Step 3: User Sign in with invalid credentials");
		loginPage.tryLoginWithInvalid(username, passwd);
	}

	@Then("user should not login")
	public void user_should_not_login() {
		System.out.println("Step 4: User login failed ,see user still on sign in page with error message");
		title = loginPage.getPageTitle();
		System.out.println("Page title is: " + title);

		System.out.println("Error message text is: " + loginPage.ErrorTextMessage());

	}

}
