package stepDefinition;

import bettingapp.SearchPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
import qa.factory.DriverFactory;

public class SearchArticleSteps {
	private static String title;
	private static String Searchtitle;
	private SearchPage searchPage = new SearchPage(DriverFactory.getDriver());


@Given("User is on search Page")
public void user_is_on_search_page() {
	System.out.println("Step 1: User is on Search Page");
	DriverFactory.getDriver().get("https://www.bbc.co.uk");

	title = searchPage.getPageTitle();
	System.out.println("Page title is: " + title);
}

@When("User search with {string}")
public void user_search_with(String article) {
	System.out.println("Step 2: User search for articles");
	searchPage.searchArticles(article);
}

@Then("articles related to sports displayed")
public void articles_related_to_sports_displayed() {
	System.out.println("Step 3: Verify articles related to sports displayed");
	Searchtitle =searchPage.getPageTitle();
	System.out.println("Search Page title is: " + title);
}

@And("see first and last heading of articles")
public void see_first_and_last_heading_of_articles() {
	System.out.println("Step 4: Verify headings of first & last articles");
	
	System.out.println("First heading on page is :-> " +searchPage.getFirstHeading());
	System.out.println("Last heading on page is :->" +searchPage.getLastHeading());
	

}
}