@SearchTest

Feature: As a sports user, I would like to read about all articles related to sports

Scenario: Use the search option to find all articles related to ‘sports’. Output the first heading and the last heading returned on the page.
    Given User is on search Page
    When User search with 'sports'
    Then articles related to sports displayed  
    And see first and last heading of articles
    