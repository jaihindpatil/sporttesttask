@matches

Feature: As a business user, I would like to make a record of all teams which are playing today

Scenario: Output all team names with a match today. If there are no matches today, output a message to convey this.

   Given User is on Scores & Fixtures - Football - BBC Sport Page
    When User select day as Today
    Then User should get all team names playing today
    And if no matches then message should be displayed